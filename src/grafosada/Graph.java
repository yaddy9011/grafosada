package grafosada;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

public class Graph {
    
    
    public String description;
   
    private Double totalWeight;
    
    public Integer vertexCount;
   
    
    public Integer edgesCount;
    
  
    private Graph dfsTree;
   
    public HashMap<String, Arista> edges;
    
    public HashMap<Integer, Vertices> vertices;
    
    public HashMap<Integer, ArrayList<Integer>> adjacencyList;
    
    public Boolean isDirected;
    
    public Boolean selfLoopsAllowed;
      

    public Graph(){
        this.vertices = new HashMap<>();
        this.edges = new HashMap();
        this.adjacencyList = new HashMap<>();
    }
    
  
    public void AddVertex(Integer id, Vertices vertex ){
        this.vertices.put(id, vertex);
    }
    
    
    public void AddEdge(String id, Arista edge){
        this.edges.put(id, edge);
        AddVertexAdjList(edge.sourceID);
        AddVertexAdjList(edge.targetID);
        
        if (!(this.adjacencyList.containsKey(edge.sourceID) && this.adjacencyList.get(edge.sourceID).contains(edge.targetID))) { 
            this.adjacencyList.get(edge.sourceID).add(edge.targetID);
        }  
        if (!(this.adjacencyList.containsKey(edge.targetID) && this.adjacencyList.get(edge.targetID).contains(edge.sourceID))) { 
            this.adjacencyList.get(edge.targetID).add(edge.sourceID);
        }
    }
    
  
    private void AddVertexAdjList(Integer id) {
        if (!this.adjacencyList.containsKey(id)) {
            this.adjacencyList.put(id, new ArrayList<>());
        }
    }
    
 
    public Graph BFS(Vertices sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        } 
        
        Vertices s = this.vertices.get(sourceVertex.id);
        Queue<Vertices> q = new LinkedList();
        this.vertices.forEach((Integer key,Vertices value) -> {
            value.explored = false;
        });
        
        s.explored = true;
        q.add(sourceVertex);
        Graph bfsTree = new Graph();
        bfsTree.vertices = this.vertices;  
        bfsTree.description = "BFSTree";
        bfsTree.isDirected = false;
        
        while(!q.isEmpty()){
            Vertices u = q.poll();

            for(Integer adj : this.adjacencyList.get(u.id)) {   
                Vertices v = this.vertices.get(adj);
                if(!v.explored) {
                    v.explored = true;
                    Arista auxEdge =  GetEdge(u.id,v.id);
                    bfsTree.AddEdge(u.id+"-"+v.id, auxEdge);
                    q.add(v);
                }
            }
        }        
        return bfsTree;
    }
    
 
    public Graph DFS_R(Vertices sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        } 
        
        Vertices source = this.vertices.get(sourceVertex.id);   
        this.dfsTree = new Graph();
        this.dfsTree.vertices = this.vertices;  
        this.dfsTree.description = "DFS_R_Tree";
        this.dfsTree.isDirected = false;
        
        this.vertices.forEach((Integer key,Vertices value) -> {
            value.parent = null;
            value.explored = false;
        });
             
        DFS_Recursive(source);
        
        return dfsTree;
    }
    
 
    private void DFS_Recursive(Vertices u) {
         u.explored = true;
         for(int adjv:this.adjacencyList.get(u.id)) {
             Vertices v = this.vertices.get(adjv);
             if (!v.explored) {  
                 v.parent = u;    
                 Arista auxEdge =  GetEdge(u.id,v.id);
                 this.dfsTree.AddEdge(u.id+"-"+v.id, auxEdge); 
                 DFS_Recursive(v);         
             }
         }
    }
    
 
    public Graph DFS_I(Vertices sourceVertex) {
        if (!this.vertices.containsKey(sourceVertex.id)) {
            throw new IllegalArgumentException("The vertex is not contained in this graph.");
        }
        
        Vertices source = this.vertices.get(sourceVertex.id);   
        
        Stack<Vertices> s = new Stack<>();
        s.add(source);
        Graph dfsTree = new Graph();
        dfsTree.vertices = this.vertices;  
        dfsTree.description = "DFS_I_Tree";
        dfsTree.isDirected = false;
        
        this.vertices.forEach((Integer key,Vertices value) -> {
            value.parent = null;
            value.explored = false;
        });
        
        while(!s.isEmpty()) {
            Vertices u = s.peek();
            if (!u.explored) {
                u.explored = true;
                if(!Objects.equals(u.id, source.id)) {
                    Arista auxEdge = GetEdge(u.id,u.parent.id);
                    dfsTree.AddEdge(u.id+"-"+u.parent.id, auxEdge);
                }

                for(Integer adj:this.adjacencyList.get(u.id)) {   
                    Vertices v = this.vertices.get(adj);
                    s.push(v);
                    v.parent = u;
                }
            } else {
                s.pop();
            }
        }
        
        return dfsTree;
    }
    

    public Graph EdgeValues(float min, float max) { 
        Random random =  new Random(); 
        
        this.edges.forEach((String key, Arista value) -> {
            value.weight = (random.nextFloat() * (max-min)) + min;
        });
        
        return this;
    }
    
    public Graph Dijkstra(Vertices source) {       
        PriorityQueue<Vertices> pqueue = new PriorityQueue<>();
        Graph path = new Graph();
        path.vertices = this.vertices;
        path.isDirected = true;
        path.edges = new HashMap<>();
        path.description = "ShortestPathFromNode" + source.id;
        
        this.vertices.forEach((Integer key, Vertices value) -> {
            value.distance = Float.MAX_VALUE;
            value.explored = false;
            value.parent = null;
        });

        this.vertices.get(source.id).distance = 0.0f;      
        pqueue.add(this.vertices.get(source.id));
        
        while(!pqueue.isEmpty()) {
            Vertices u = pqueue.remove();
            u.explored = true;
            
            if(u.parent != null){
                Arista pathEdge = new Arista();
                pathEdge.sourceID = u.parent.id;
                pathEdge.targetID = u.id;
                pathEdge.weight = GetEdge(pathEdge.sourceID,pathEdge.targetID).weight;
                path.AddEdge(pathEdge.sourceID+"-"+pathEdge.targetID, pathEdge);
            }
                 
            for (Integer value : this.adjacencyList.get(u.id)){
                Vertices v = this.vertices.get(value);
                if(!v.explored && (v.distance > u.distance + GetEdge(u.id,value).weight)){  
                    v.distance = u.distance + GetEdge(u.id,value).weight;
                    v.parent = u;
                    if (pqueue.contains(v)){
                        pqueue.remove(v);
                    }
                    pqueue.add(v);
                }
            }
        }
        
        return path;
    }
    
   
    private boolean IsConnected(){

        DFS_R(new Vertices(1));
        
        for(Vertices v: this.vertices.values()){
            if(!v.explored){
                return false;
            }
        }
        
        return true;
    }
    

    public Graph Kruskal_D(){
        PriorityQueue<Arista> pqueue = new PriorityQueue<>();
        Graph MST = new Graph();
        MST.description =  "KruskalD_MST";
        MST.vertices = this.vertices;
        MST.isDirected = false; 
        MST.totalWeight=0.0;
        UnionFind UF = new UnionFind(new HashSet<>(this.vertices.values())); 
        
        this.edges.forEach((String key,Arista value) -> {
            pqueue.add(value);
        });
        
        for (int i=0; i<this.edges.size(); i++) {
            Arista e = pqueue.remove();
            if(!UF.InSameSet(this.vertices.get(e.sourceID), this.vertices.get(e.targetID))){
                MST.AddEdge(e.sourceID+"-"+e.targetID, e);
                MST.totalWeight += e.weight;
                UF.Union(this.vertices.get(e.sourceID), this.vertices.get(e.targetID));
            }
        }
        
        System.out.println("Peso del MST generado por Kruskal directo: " + MST.totalWeight);
        return MST;
    }
    

    public Graph Kruskal_I(){

        Graph MST = new Graph();
        MST.description = "KruskalI_MST";
        MST.totalWeight = 0.0;
        MST.isDirected = false;
        
        this.vertices.forEach((Integer key, Vertices value) -> {
            MST.AddVertex(key, value);
        });
        
        this.edges.forEach((String key, Arista value) -> {
            MST.AddEdge(key, value);
        });

        ArrayList<Arista> orderedEdges = new ArrayList(this.edges.values());
        Collections.sort(orderedEdges, Collections.reverseOrder());
        
        for (Arista e: orderedEdges) {         
            MST.adjacencyList.get(e.sourceID).remove(e.targetID); 
            MST.adjacencyList.get(e.targetID).remove(e.sourceID); 
            if(!MST.IsConnected()){
                MST.adjacencyList.get(e.sourceID).add(e.targetID);
                MST.adjacencyList.get(e.targetID).add(e.sourceID);                
            } else {
                if(MST.edges.containsKey(e.sourceID+"-"+e.targetID)){
                    MST.edges.remove(e.sourceID+"-"+e.targetID);
                }
                if(MST.edges.containsKey(e.targetID+"-"+e.sourceID)){
                    MST.edges.remove(e.targetID+"-"+e.sourceID);
                }
            }            
        }
        
        MST.edges.forEach((String key, Arista value) -> {
            MST.totalWeight += value.weight;
        });
        
        System.out.println("Peso del MST generado por Kruskal inverso: " + MST.totalWeight);
        
        return MST;
    }
    
 
    public Graph Prim() {
        PriorityQueue<Vertices> pqueue = new PriorityQueue<>();
        Graph mst = new Graph();
        mst.vertices = this.vertices;
        mst.isDirected = false;
        mst.edges = new HashMap<>();
        mst.description = "Prim_MST";
        mst.totalWeight = 0.0;

        this.vertices.forEach((Integer key, Vertices value) -> {
            value.distance = Float.POSITIVE_INFINITY;
            value.explored = false;
            value.parent = null;
            pqueue.add(value);
        });
        
        while(!pqueue.isEmpty()) {
            Vertices u = pqueue.remove();
            u.explored = true;
            
            if(u.parent!=null){
                Arista mstEdge = new Arista();
                mstEdge.sourceID = u.parent.id;
                mstEdge.targetID = u.id;
                mstEdge.weight = GetEdge(mstEdge.sourceID,mstEdge.targetID).weight;
                mst.AddEdge(mstEdge.sourceID+"-"+mstEdge.targetID, mstEdge);
                mst.totalWeight += mstEdge.weight;
            }
                 
            for (Integer value : this.adjacencyList.get(u.id)) {  
                Vertices v = this.vertices.get(value);
                if(!v.explored && (v.distance > GetEdge(u.id,value).weight)){
                    v.distance = GetEdge(u.id,value).weight;
                    v.parent = u;
                    if (pqueue.contains(v)){
                        pqueue.remove(v);
                    }
                    pqueue.add(v);
                }
            }
        }
        
        System.out.println("Peso del MST generado por Prim: " + mst.totalWeight);
        
        return mst;
    }
    
 
    public Arista GetEdge(Integer node1, Integer node2) {
        String edgeKey = this.edges.containsKey(node1+"-"+node2) ? node1+"-"+node2 : "" ;
        edgeKey = this.edges.containsKey(node2+"-"+node1) ? node2+"-"+node1 : edgeKey;
        
        return this.edges.containsKey(edgeKey) ? this.edges.get(edgeKey) : null;
    }
    
   
    public void PrintToFile(boolean showDistance, boolean showVertexWeight, boolean ShowTotalWeight, String name) throws FileNotFoundException{
                
        PrintWriter writter = new PrintWriter(name+".gv");
        String separator;
        if(this.isDirected){
            writter.print("digraph ");
            separator = " -> ";
        }
        else
        {
            writter.print("graph ");
            separator = " -- ";
        }
        
        writter.print(this.description);
        writter.print("{");
        
        this.vertices.forEach((Integer key, Vertices value) -> {
            String idVertex = value.id.toString();
            String label = " [label = \"Nodo"+idVertex;
            if(showDistance){
                label = label +" ("+value.distance+")";
            }
            if(ShowTotalWeight&&(value.id==1)){               
                label = " [label = \"PesoTotal = "+this.totalWeight;
            }              
            writter.println(idVertex+label+"\"];");
        });
        
        this.edges.forEach((String key, Arista value) -> {
            String source = value.sourceID.toString();
            String target  = value.targetID.toString();
            String label = "";
            if(showVertexWeight){
                label = " [label = \""+value.weight+"\"]";
            }
            writter.println(source+separator+target+label+";");
        });
        
        writter.print("}");
        writter.close();      
    }
}
