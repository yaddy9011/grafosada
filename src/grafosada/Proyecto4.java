
package grafosada;

/**
 *
 * @author JazCast
 */
public class Proyecto4 {
    
    public static void main(String[] args) {
        
        try {
            
            Graph gil10 = Generador.ArquetipoGraphGilbert(10, 0.45, false);
            Graph wgGil10 = gil10.EdgeValues(1, 100);
            wgGil10.PrintToFile(false, true, false, "Gilbert10");
            Graph kdGil10 = wgGil10.Kruskal_D();
            kdGil10.PrintToFile(false, true, true, "Gilbert10_MST_KD");
            Graph kiGil10 = wgGil10.Kruskal_I();
            kiGil10.PrintToFile(false, true, true, "Gilbert10_MST_KI");
            Graph primGil10 = wgGil10.Prim();
            primGil10.PrintToFile(false, true, true, "Gilbert10_MST_Prim");

            Graph gil150 = Generador.ArquetipoGraphGilbert(100, 0.45, false);
            Graph wgGil150 = gil150.EdgeValues(1, 100);
            wgGil150.PrintToFile(false, false, false, "Gilbert100");
            Graph kdGil150 = wgGil150.Kruskal_D();
            kdGil150.PrintToFile(false, true, true, "Gilbert100_MST_KD");
            Graph kiGil150 = wgGil150.Kruskal_I();
            kiGil150.PrintToFile(false, true, true, "Gilbert100_MST_KI");
            Graph primGil150 = wgGil150.Prim();
            primGil150.PrintToFile(false, true, true, "Gilbert100_MST_Prim");

            Graph erdos10 = Generador.ArquetipoErdosRenyi(10, 15, false);
            Graph wgErdos10 = erdos10.EdgeValues(1, 100);
            wgErdos10.PrintToFile(false, true, false, "Erdos10");
            Graph kdErdos10 = wgErdos10.Kruskal_D();
            kdErdos10.PrintToFile(false, true, true, "Erdos10_MST_KD");
            Graph kiErdos10 = wgErdos10.Kruskal_I();
            kiErdos10.PrintToFile(false, true, true, "Erdos10_MST_KI");
            Graph primErdos10 = wgErdos10.Prim();
            primErdos10.PrintToFile(false, true, true, "Erdos10_MST_Prim");

            Graph erdos150 = Generador.ArquetipoErdosRenyi(100, 500, false);
            Graph wgErdos150 = erdos150.EdgeValues(1, 100);
            wgErdos150.PrintToFile(false, true, false, "Erdos100");
            Graph kdErdos150 = wgErdos150.Kruskal_D();
            kdErdos150.PrintToFile(false, true, true, "Erdos100_MST_KD");
            Graph kiErdos150 = wgErdos150.Kruskal_I();
            kiErdos150.PrintToFile(false, true, true, "Erdos100_MST_KI");
            Graph primErdos150 = wgErdos150.Prim();
            primErdos150.PrintToFile(false, true, true, "Erdos100_MST_Prim");

            Graph barabasi10 = Generador.ArquetipoErdosRenyi(10, 5, false);
            Graph wgBarabasi10 = barabasi10.EdgeValues(1, 100);
            wgBarabasi10.PrintToFile(false, true, false, "Barabasi10");
            Graph kdBarabasi10 = wgBarabasi10.Kruskal_D();
            kdBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_KD");
            Graph kiBarabasi10 = wgBarabasi10.Kruskal_I();
            kiBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_KI");
            Graph primBarabasi10 = wgBarabasi10.Prim();
            primBarabasi10.PrintToFile(false, true, true, "Barabasi10_MST_Prim");

            Graph barabasi150 = Generador.ArquetipoBarabasiAlbertGraph(100, 4, false);
            Graph wgBarabasi150 = barabasi150.EdgeValues(1, 100);
            wgBarabasi150.PrintToFile(false, true, false, "Barabasi100");
            Graph kdBarabasi150 = wgBarabasi150.Kruskal_D();
            kdBarabasi150.PrintToFile(false, true, true, "Barabasi100_MST_KD");
            Graph kiBarabasi150 = wgBarabasi150.Kruskal_I();
            kiBarabasi150.PrintToFile(false, true, true, "Barabasi100_MST_KI");
            Graph primBarabasi150 = wgBarabasi150.Prim();
            primBarabasi150.PrintToFile(false, true, true, "Barabasi100_MST_Prim");

            Graph geo10 = Generador.ArquetipoGeographicalGraph(10, 0.5, false);
            Graph wgGeo10 = geo10.EdgeValues(1, 100);
            wgGeo10.PrintToFile(false, true, false, "Geo10");
            Graph kdGeo10 = wgGeo10.Kruskal_D();
            kdGeo10.PrintToFile(false, true, true, "Geo10_MST_KD");
            Graph kiGeo10 = wgGeo10.Kruskal_I();
            kiGeo10.PrintToFile(false, true, true, "Geo10_MST_KI");
            Graph primGeo10 = wgGeo10.Prim();
            primGeo10.PrintToFile(false, true, true, "Geo10_MST_Prim");

            Graph geo150 = Generador.ArquetipoGeographicalGraph(100, 0.2, false);
            Graph wgGeo150 = geo150.EdgeValues(1, 100);
            wgGeo150.PrintToFile(false, true, false, "Geo100");
            Graph kdGeo150 = wgGeo150.Kruskal_D();
            kdGeo150.PrintToFile(false, true, true, "Geo100_MST_KD");
            Graph kiGeo150 = wgGeo150.Kruskal_I();
            kiGeo150.PrintToFile(false, true, true, "Geo100_MST_KI");
            Graph primGeo150 = wgGeo150.Prim();
            primGeo150.PrintToFile(false, true, true, "Geo100_MST_Prim");

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
  
     
    }
    
}
