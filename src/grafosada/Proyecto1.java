/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosada;

/**
 *
 * @author JazCast
 */
public class Proyecto1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {

                  // Modelo de Erros 
            Graph ErA = Generador.ArquetipoErdosRenyi(20, 95, false);
            ErA.PrintToFile(false, false, false, "er20");

            Graph ErB = Generador.ArquetipoErdosRenyi(100, 2495, false);
            ErA.PrintToFile(false, false, false, "er100");

            Graph ErC = Generador.ArquetipoErdosRenyi(500, 62375, false);
            ErA.PrintToFile(false, false, false, "er500");

                 // MODELO DE Gilbert.
            Graph GilA = Generador.ArquetipoGraphGilbert(20, 0.45, false);
            GilA.PrintToFile(false, false, false, "gilbert");

            Graph GilB = Generador.ArquetipoGraphGilbert(100, 0.45, false);
            GilB.PrintToFile(false, false, false, "gilbert100");

            Graph GilC = Generador.ArquetipoGraphGilbert(500, 0.45, false);
            GilC.PrintToFile(false, false, false, "gilbert500");

                   // MODELO DE BARABASI
            Graph BaraA = Generador.ArquetipoBarabasiAlbertGraph(20, 5, false);
            BaraA.PrintToFile(false, false, false, "Barabasi20");

            Graph BaraB = Generador.ArquetipoBarabasiAlbertGraph(100, 25, false);
            BaraB.PrintToFile(false, false, false, "Barabasi100");

            Graph BaraC = Generador.ArquetipoBarabasiAlbertGraph(500, 100, false);
            BaraC.PrintToFile(false, false, false, "Barabasi500");

                       // MODELO GEOGRAFICO
            
            Graph GeoA = Generador.ArquetipoGeographicalGraph(20, 0.5, false);
            GeoA.PrintToFile(false, false, false, "Geo20");

            Graph GeoB = Generador.ArquetipoGeographicalGraph(100, 0.2, false);
            GeoB.PrintToFile(false, false, false, "Geo100");

            Graph GeoC = Generador.ArquetipoGeographicalGraph(500, 0.2, false);
            GeoC.PrintToFile(false, false, false, "Geo500");

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }

    }

}
