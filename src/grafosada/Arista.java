package grafosada;


public class Arista implements Comparable<Arista> { 
    
    /**
     * Identifier of the edge
     */
    public Integer Id;
    
    /**
     * Arista source Id 
     */
    public Integer sourceID;
    
    /**
     * Arista destination Id
     */
    public Integer targetID ;
    
    /**
     * weight on the edge
     */
    public Float weight;
    
    @Override
    public int compareTo(Arista other) {  
        return this.weight > other.weight ? 1 : this.weight < other.weight? -1 : 0;  
    }
}
