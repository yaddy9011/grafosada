package grafosada;

import java.util.HashMap;
import java.util.Random;

public class Generador {
    
 
    public static Graph ArquetipoGraphGilbert(Integer vertexNumber, Double probability, Boolean selfLoopAllowed) {
    
        if(probability< 0.0 || probability>1.0){
           throw new IllegalArgumentException("Probability must be less or equal than 1.0 and greater than 0");
        }
        
        Graph result = new Graph();
        result.description = "Gilbert";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        
        int startingPoint = selfLoopAllowed ? 0 : 1;
        for(int vertex = 1; vertex <= vertexNumber;vertex++){
            Vertices vertexAux = new Vertices(vertex);
            result.AddVertex(vertex, vertexAux);
            for(int nextVertex = vertex + startingPoint; nextVertex<=vertexNumber; nextVertex++){
                if(IsSuccess(probability)){
                    Arista edgeAux = new Arista();
                    edgeAux.sourceID = vertex;
                    edgeAux.targetID = nextVertex;
                    result.AddEdge(vertex+"-"+nextVertex, edgeAux);  
                }
            }
        }
        
        return result;
    }
        
    public static Graph ArquetipoErdosRenyi(Integer vertexNumber, Integer edgeNumber, Boolean selfLoopAllowed) {
        if(vertexNumber == 0 || edgeNumber == 0){
            throw new IllegalArgumentException("Number of vertices and edges must be greater than zero");
        }
        
        if((vertexNumber*vertexNumber)<(edgeNumber)){
            throw new IllegalArgumentException("Invalid number of edges.");
        }
    
        //Create the vertices
        Graph result = new Graph();
        result.description = "ErdosRenyi";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();
        for(int vertexCreated = 1; vertexCreated <= vertexNumber;vertexCreated++){
            Vertices item = new Vertices(vertexCreated);
            result.AddVertex(vertexCreated, item);
        }
        
        //Creates the edges
        result.edges = new HashMap();
        for(int edgeCreated = 1; edgeCreated <= edgeNumber;edgeCreated++){
            Integer source = CreateRandomInteger(1,vertexNumber);
            Integer target = CreateRandomInteger(1,vertexNumber);
            
            if(!ExistOnGraph(result, source, target)) {
                if(!selfLoopAllowed && (source.equals(target))) {
                    edgeCreated--;
                } else {
                    Arista newEdge = new Arista();
                    newEdge.sourceID = source;
                    newEdge.targetID = target;
                    result.AddEdge(source+"-"+target, newEdge);
                }
            } else {
                edgeCreated--;
            }
        }
        
        return result;
    }
    
    public static Graph ArquetipoGeographicalGraph(Integer vertexNumber, Double distance, Boolean selfLoopAllowed) {
        Graph result = new Graph();
        result.description = "Geographical";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();
        
        for(int vertexCreated = 1; vertexCreated <=vertexNumber;vertexCreated++){
            Vertices item = new Vertices(vertexCreated);
            item.X = CreateRandomNumber();
            item.Y = CreateRandomNumber();
            result.AddVertex(vertexCreated, item);
        }
         
        int startingPoint = selfLoopAllowed ? 0 : 1;
        //Create the edges
        result.edges = new HashMap();
        for(int i = 1; i <= vertexNumber; i++){
            for(int j = i + startingPoint; j<= vertexNumber; j++){
                Double euclideanDistance = Math.sqrt(Math.pow(result.vertices.get(i).X - result.vertices.get(j).X,2) + Math.pow(result.vertices.get(i).Y - result.vertices.get(j).Y,2));
                if(euclideanDistance <= distance) {
                    Arista newEdge = new Arista();
                    newEdge.sourceID = i;
                    newEdge.targetID = j;
                    result.AddEdge(i+"-"+j, newEdge);
                }
            }          
        }

        
        return result;
    }
    
    public static Graph ArquetipoBarabasiAlbertGraph(Integer vertexNumber, Integer degreeV, Boolean selfLoopAllowed) {
        Graph result = new Graph();
        result.description = "BarabasiAlbert";
        result.isDirected = false;
        result.selfLoopsAllowed = selfLoopAllowed;
        result.vertices = new HashMap<>();

        //Create the edges
        result.edges = new HashMap();
        double p = 0.0;
        for(int i = 1; i <= vertexNumber; i++){
            Vertices item = new Vertices(i);
            item.degree = 0;
            result.AddVertex(i, item);
            for(int j = 1; j<= result.vertices.size(); j++){
                if(result.vertices.get(i).degree<=degreeV){
                    if((i==j)&&!selfLoopAllowed) {
                        continue;
                    } else {
                        p = 1 - (result.vertices.get(j).degree/Double.valueOf(degreeV));
                        if (CreateRandomNumber() < p) {
                            Arista newEdge = new Arista();
                            newEdge.sourceID = i;
                            newEdge.targetID = j;
                            result.AddEdge(i+"-"+j, newEdge);
                            result.vertices.get(i).degree += 1;
                            if(i!=j){
                                result.vertices.get(j).degree += 1;
                            }
                        }
                    }
                } else {
                    break;
                }
            }          
        } 
        
        return result;
    }
     
    public static Boolean ExistOnGraph(Graph graph, Integer source, Integer target){       
        Boolean exist = graph.edges.containsKey(source+"-"+target);
        Boolean existBackwards = graph.edges.containsKey(target+"-"+source);
            
        return (exist || existBackwards);
    }
    
    private static double CreateRandomNumber() {
        Random random = new Random();
        return random.nextDouble();
    }
 
    private static int CreateRandomInteger(Integer minNumber, Integer maxNumber){
         Random random =  new Random();
         return random.nextInt((maxNumber - minNumber) + 1) + minNumber;
     }   
     
    private static boolean IsSuccess(double p) {
        if (!(p >= 0.0 && p <= 1.0))
            throw new IllegalArgumentException("invalid probability 2");
        return CreateRandomNumber() < p;
    }
    
}
