package grafosada;

import java.util.*;

public class UnionFind<T>
{
    /**
     * Parent map 
     */
    private Map<T, T> parent;
    
    /**
     * Rank map
     */
    private Map<T, Integer> rank;
    
    /**
     * Number of components in the structure
     */
    private int components; 

    /**
     * Creates a UnionFind instance and initializes it.
     * @param elements All the elements of the UnionFind structure.
     */
    public UnionFind(Set<T> elements)
    {
        parent = new LinkedHashMap<>();
        rank = new HashMap<>();
        for (T element : elements) {
            parent.put(element, element);
            rank.put(element, 0);
        }
        components = elements.size();
    }

    /**
     * Determine which subset an element is in. 
     * @param element The element to find.
     * @return The root of the set the element is in.
     */
    public T Find(T element)
    {
        if (!parent.containsKey(element)) {
            throw new IllegalArgumentException("The element is not contained in this UnionFind structure");
        }

        T current = element;
        while (true) {
            T parent = this.parent.get(current);
            if (parent.equals(current)) {
                break;
            }
            current = parent;
        }
        T root = current;

        current = element;
        while (!current.equals(root)) {
            T parent = this.parent.get(current);
            this.parent.put(current, root);
            current = parent;
        }

        return root;
    }

    /**
     * Merges the sets which contain element1 and element2. 
     * @param element1 The first element
     * @param element2 The second element
     */
    public void Union(T element1, T element2)
    {
        if (!parent.containsKey(element1) || !parent.containsKey(element2)) {
            throw new IllegalArgumentException("One or both elements could not be found in the UnioFind structure.");
        }

        T parent1 = Find(element1);
        T parent2 = Find(element2);

        if (parent1.equals(parent2)) {
            return;
        }

        int rank1 = rank.get(parent1);
        int rank2 = rank.get(parent2);
        if (rank1 > rank2) {
            parent.put(parent2, parent1);
        } else if (rank1 < rank2) {
            parent.put(parent1, parent2);
        } else {
            parent.put(parent2, parent1);
            rank.put(parent1, rank1 + 1);
        }
        components--;
    }

    /**
     * Tests whether two elements are contained in the same set.
     * 
     * @param element1 first element
     * @param element2 second element
     * @return true if element1 and element2 are contained in the same set, false otherwise.
     */
    public boolean InSameSet(T element1, T element2)
    {
        return Find(element1).equals(Find(element2));
    }
}