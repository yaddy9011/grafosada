/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grafosada;

/**
 *
 * @author JazCast
 */
public class Proyecto2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

            try { 
                
            Graph result = Generador.ArquetipoGraphGilbert(20, 0.55, false);
            result.PrintToFile(false, false, false, "Graph");
            Vertices source = new Vertices(1);
            Graph bfs = result.BFS(source);
            bfs.PrintToFile(false, false, false, "BFSTree");
            Graph dfsI = result.DFS_I(source);
            dfsI.PrintToFile(false, false, false, "DFS_I_Tree");
            Graph dfsR = result.DFS_R(source);
            dfsR.PrintToFile(false, false, false, "DFS_R_Tree");
        
    }   catch (Exception ex) {
            System.out.print(ex.getMessage());
        }


}
    
}
