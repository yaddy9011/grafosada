package grafosada;

/**
 *
 * @author JazCast
 */
public class Proyecto3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {

            Vertices s = new Vertices(1);
            Graph gil10 = Generador.ArquetipoGraphGilbert(10, 0.45, false);
            Graph gil150 = Generador.ArquetipoGraphGilbert(150, 0.45, false);
            Graph wgGil10 = gil10.EdgeValues(1, 100);
            Graph wgGil150 = gil150.EdgeValues(1, 100);
            wgGil10.PrintToFile(false, true, false, "Gilbert10");
            wgGil150.PrintToFile(false, false, false, "Gilbert150");
            Graph pathGil10 = wgGil10.Dijkstra(s);
            Graph pathGil150 = wgGil150.Dijkstra(s);
            pathGil10.PrintToFile(true, true, false, "Gilbert10_DijkstraTree");
            pathGil150.PrintToFile(true, true, false, "Gilbert150_DijkstraTree");

            Graph erdos10 = Generador.ArquetipoErdosRenyi(10, 15, false);
            Graph erdos150 = Generador.ArquetipoErdosRenyi(150, 500, false);
            Graph wgErdos10 = erdos10.EdgeValues(1, 100);
            Graph wgErdos150 = erdos150.EdgeValues(1, 100);
            wgErdos10.PrintToFile(false, true, false, "Erdos10");
            wgErdos150.PrintToFile(false, false, false, "Erdos150");
            Graph pathErdos10 = wgErdos10.Dijkstra(s);
            Graph pathErdos150 = wgErdos150.Dijkstra(s);
            pathErdos10.PrintToFile(true, true, false, "Erdos10_Dijkstra");
            pathErdos150.PrintToFile(true, true, false, "Erdos150_Dijkstra");

            Graph barabasi10 = Generador.ArquetipoBarabasiAlbertGraph(10, 5, false);
            Graph barabasi50 = Generador.ArquetipoBarabasiAlbertGraph(150, 4, false);
            Graph wgBarabasi10 = barabasi10.EdgeValues(1, 100);
            Graph wgBarabasi150 = barabasi50.EdgeValues(1, 100);
            wgBarabasi10.PrintToFile(false, true, false, "Barabasi10");
            wgBarabasi150.PrintToFile(false, true, false, "Barabasi150");
            Graph pathBarabasi10 = wgBarabasi10.Dijkstra(s);
            Graph pathBarabasi150 = wgBarabasi150.Dijkstra(s);
            pathBarabasi10.PrintToFile(true, true, false, "Barabasi10_Dijkstra");
            pathBarabasi150.PrintToFile(true, true, false, "Barabasi150_Dijkstra");

            Graph geo10 = Generador.ArquetipoGeographicalGraph(10, 0.5, false);
            Graph geo150 = Generador.ArquetipoGeographicalGraph(150, 0.2, false);
            Graph wgGeo10 = geo10.EdgeValues(1, 100);
            Graph wgGeo150 = geo150.EdgeValues(1, 100);
            wgGeo10.PrintToFile(false, true, false, "Geo10");
            wgGeo150.PrintToFile(false, true, false, "Geo150");
            Graph pathGeo10 = wgGeo10.Dijkstra(s);
            Graph pathGeo150 = wgGeo150.Dijkstra(s);
            pathGeo10.PrintToFile(true, true, false, "Geo10_Dijkstra");
            pathGeo150.PrintToFile(true, true, false, "Geo150_Dijkstra");

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }

    }

}
