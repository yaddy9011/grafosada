package grafosada;

public class Vertices implements Comparable<Vertices> {
    
    /**
     * Value or identifier on the node, 
     */
    public Integer id;
    
    /**
     * Number of Edges connected to a node
     * deg(node) 
     */
    public Integer degree;
    
    /**
     * distance from a source node
     */
    public Float distance;
    
    /**
     * description of the node
     */
    public String description;
    
    /**
     * Indicates whether the vertex has been explored or not
     */
    public Boolean explored;
    
    /**
     * parent of vertex
     */
    public Vertices parent;
   
    /**
     * Position on x 
     */
    public Double X;
    
    /**
     * Position on y
     */
    public Double Y;
    
    /**
     * Position on z
     */
    public Integer z;
    
    /**
     * Builder of vertex
     */
    public Vertices(int id){
        this.id = id;
    }
    
    @Override
    public int compareTo(Vertices other) {
        if (this.distance < other.distance){
            return -1;
        } else if (this.distance > other.distance){
            return 1;
        } else {
          return 0;
        }
    }
}
